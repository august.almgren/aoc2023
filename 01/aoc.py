# note - this file is hard linked locally (not supported in git)

from operator import add, neg
from functools import reduce
import requests
import os

# load input as lines from input.txt, or from aoc website & write to input.txt if it doesn't exist
# the year and day to load for is determined based on the current dir (should be .../year/day/)
# note - the session id needs to be saved in ./sesid.txt __without__ a newline at the end
def loadinput():
	input_fname = 'input.txt'
	sesid_fname = 'sesid.txt'

	if not os.path.exists(input_fname):

		cwdirs = os.getcwd().split('/')
		year = int(cwdirs[-2])
		day = int(cwdirs[-1])
		url = 'https://adventofcode.com/{}/day/{}/input'.format(year, day)

		cookies = requests.cookies.RequestsCookieJar()
		sesidfile = open(sesid_fname)
		sesid = sesidfile.read()
		sesidfile.close()
		cookies.set('session', sesid)

		inputdata = requests.get(url, cookies=cookies).text
		inputfile = open(input_fname, 'w')
		inputfile.write(inputdata)
		inputfile.close()
	else:
		inputfile = open(input_fname)
		inputdata = inputfile.read()
		inputfile.close()

	return inputdata.splitlines()

# iterator for splitting a list into equally sized chunks (last chunk may be smaller)
class chunk:
	def __init__(self, data, chunksize):
		self.data = data
		self.chunksize = chunksize
		self.len = len(data)

	def __iter__(self):
		self.i = 0
		return self

	def __next__(self):
		if self.i < self.len:
			nextchunk = self.data[self.i:self.i + self.chunksize]
			self.i += self.chunksize
			return nextchunk
		else:
			raise StopIteration

# iterator for splitting a list into n identically sized chunks (last chunk may be smaller)
def split(data, n):
	return chunk(data, int(len(data) / n))

# given a list of kv-tuples, return a dict mapping keys to grouped lists of values
# send True as 2nd param to remove duplicates from values
def group(kvs):
	groups = {}
	for k,v in kvs:
		groups.setdefault(k,[]).append(v)
	return groups

dirs = {'n':[0,1],'ne':[1,1],'e':[1,0],'se':[1,-1],'s':[0,-1],'sw':[-1,-1],'w':[-1,0],'nw':[-1,1]}
dirs = {**dirs, 'u':[0,1],'ur':[1,1],'r':[1,0],'dr':[1,-1],'d':[0,-1],'dl':[-1,-1],'l':[-1,0],'ul':[-1,1]}

def clamp(n, a, b):
	return max(min(n, b), a)

def gcd(*kwargs):
	return reduce(lambda a, b: a if b == 0 else gcd(b, a % b), kwargs)

def lcm(*kwargs):
	return reduce((a * b) / gcd(a, b), kwargs)

def vadd(a, b):
	return map(sum, zip(a, b))

def vsub(a, b):
	return vadd(a, map(neg, b))
